# config valid only for current version of Capistrano
lock "3.7.1"
# 设置部署项目的名字
set :application, 'science_data'
# 设置项目的 git repo
set :repo_url, 'git@git.oschina.net:bomb/lala.git'
# 设置部署目录
set :deploy_to, "/opt/nginx/html/#{ fetch(:application) }"
# 设置部署时的用户
set :deploy_user, 'vagrant'
set :unicorn_user, 'vagrant'
set :nginx_location, '/opt/nginx'

set :scm, :git
set :format, :pretty
set :pty, true
# linked_files
# 它是将 shared 目录中的文件 link 到项目中, 文件要首先存在于 shared 目录中, 不然 deploy 时会报错
# 在 Rails 项目中, 主要就是 database.yml, secret.yml 这样的敏感文件
set :linked_files, %w{config/database.yml config/secrets.yml}

# linked_dirs
# 是将项目的指定目录 link 到 shared 目录中, 这个操作会在从 repo 取下代码之后进行.
# 比如 log 目录, 每次部署完毕后新的版本中的 log 目录都会 link 到 shared/log 下
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

set :keep_releases, 10

namespace :deploy do

  # 自定义了一个部署任务, 即自动运行 rake RAILS_ENV=rails_env db:create
  # 其中 release_path 指的是当前 release 目录
  # `fetch(:rails_env)` 读取配置中的 rails_env 变量, 并在 rake 命令中带上 env 变量
  task :create_database do
    on roles(:db) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'db:create'
        end
      end
    end
  end

  before :migrate, :create_database     # 在每次 rake db:migrate 前都运行 rake db:create
  after  :finishing, 'deploy:cleanup'   # 在每次部署完毕后, 清理部署时生成的 tmp 文件及多余的版本
end
