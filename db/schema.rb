# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161227074144) do

  create_table "achievement_services", force: :cascade do |t|
    t.integer  "school_id",        limit: 4
    t.string   "record_year",      limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "hj_xms",           limit: 4
    t.integer  "hj_dntrrs",        limit: 4
    t.integer  "hj_zdyjs",         limit: 4
    t.integer  "hj_dnbrjf",        limit: 4
    t.integer  "hj_dnzcjf",        limit: 4
    t.integer  "yjyfzcgyy_xms",    limit: 4
    t.integer  "yjyfzcgyy_dntrrs", limit: 4
    t.integer  "yjyfzcgyy_zdyjs",  limit: 4
    t.integer  "yjyfzcgyy_dnbrjf", limit: 4
    t.integer  "yjyfzcgyy_dnzcjf", limit: 4
    t.integer  "kjfw_xms",         limit: 4
    t.integer  "kjfw_dntrrs",      limit: 4
    t.integer  "kjfw_zdyjs",       limit: 4
    t.integer  "kjfw_dnbrjf",      limit: 4
    t.integer  "kjfw_dnzcjf",      limit: 4
  end

  create_table "activity_profiles", force: :cascade do |t|
    t.integer  "school_id",                 limit: 4
    t.string   "record_year",               limit: 255
    t.integer  "jxykyry_total",             limit: 4
    t.integer  "jxykyry_kxj_gcs_total",     limit: 4
    t.integer  "jxykyry_kxj_gcs_gjzc",      limit: 4
    t.integer  "yjyfzry_total",             limit: 4
    t.integer  "yjyfzry_kxj_gcs_total",     limit: 4
    t.integer  "yjyfzry_kxj_gcs_gjzc",      limit: 4
    t.integer  "yjyfzry_qsdlry",            limit: 4
    t.integer  "kjjf_total",                limit: 4
    t.integer  "kjjf_dnbr_total",           limit: 4
    t.integer  "kjjf_dnbr_zfzj",            limit: 4
    t.integer  "kjjf_dnbr_qsydwwt",         limit: 4
    t.integer  "kjjf_dnbr_other",           limit: 4
    t.integer  "kjjf_dnnbzc",               limit: 4
    t.integer  "kjkt_ktzs",                 limit: 4
    t.integer  "kjkt_dntrrs",               limit: 4
    t.integer  "kjkt_dnbrjf",               limit: 4
    t.integer  "kjkt_dnzcjf",               limit: 4
    t.integer  "kjcgjjszr_zz_sl",           limit: 4
    t.integer  "kjcgjjszr_zz_zs",           limit: 4
    t.integer  "kjcgjjszr_xslw_total",      limit: 4
    t.integer  "kjcgjjszr_xslw_gwjqgxkwfb", limit: 4
    t.integer  "kjcgjjszr_jdcgs",           limit: 4
    t.integer  "kjcgjjszr_jszr_qdhts",      limit: 4
    t.integer  "kjcgjjszr_jszr_dnsjsr",     limit: 4
    t.integer  "cgsj_total",                limit: 4
    t.integer  "cgsj_gjjj",                 limit: 4
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "assignments", force: :cascade do |t|
    t.integer  "school_id",    limit: 4
    t.string   "record_year",  limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "hts_total",    limit: 4
    t.integer  "hts_gyqy",     limit: 4
    t.integer  "hts_wzqy",     limit: 4
    t.integer  "hts_myqy",     limit: 4
    t.integer  "hts_other",    limit: 4
    t.integer  "htje_total",   limit: 4
    t.integer  "htje_gyqy",    limit: 4
    t.integer  "htje_wzqy",    limit: 4
    t.integer  "htje_myqy",    limit: 4
    t.integer  "htje_other",   limit: 4
    t.integer  "dnsjsr_total", limit: 4
    t.integer  "dnsjsr_gyqy",  limit: 4
    t.integer  "dnsjsr_wzqy",  limit: 4
    t.integer  "dnsjsr_myqy",  limit: 4
    t.integer  "dnsjsr_other", limit: 4
  end

  create_table "awards", force: :cascade do |t|
    t.integer  "school_id",       limit: 4
    t.string   "record_year",     limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "gjzgkxjsj_total", limit: 4
    t.integer  "gjzgkxjsj_td",    limit: 4
    t.integer  "gjzrkxj_total",   limit: 4
    t.integer  "gjzrkxj_yd",      limit: 4
    t.integer  "gjzrkxj_ed",      limit: 4
    t.integer  "gjfmj_total",     limit: 4
    t.integer  "gjfmj_yd",        limit: 4
    t.integer  "gjfmj_ed",        limit: 4
    t.integer  "gjkjjbj_total",   limit: 4
    t.integer  "gjkjjbj_td",      limit: 4
    t.integer  "gjkjjbj_yd",      limit: 4
    t.integer  "gjkjjbj_ed",      limit: 4
    t.integer  "gjkjjbj_sd",      limit: 4
    t.integer  "gwygbm_kjjbj",    limit: 4
    t.integer  "sszzq_kjjbj",     limit: 4
  end

  create_table "communications", force: :cascade do |t|
    t.integer  "school_id",   limit: 4
    t.string   "record_year", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "hzjl_pq",     limit: 4
    t.integer  "hzjl_js",     limit: 4
    t.integer  "gjxshy_cxry", limit: 4
    t.integer  "hzjl_jllw",   limit: 4
    t.integer  "hzjl_tybg",   limit: 4
    t.integer  "hzjl_zb",     limit: 4
  end

  create_table "man_duties", force: :cascade do |t|
    t.integer  "school_id",   limit: 4
    t.string   "record_year", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "total",       limit: 4
    t.integer  "jsxl_xj",     limit: 4
    t.integer  "jsxl_jshi",   limit: 4
    t.integer  "jsxl_fjs",    limit: 4
    t.integer  "jsxl_jshou",  limit: 4
    t.integer  "jsxl_zs",     limit: 4
    t.integer  "jsxl_other",  limit: 4
    t.integer  "qtzwxl_xj",   limit: 4
    t.integer  "qtzwxl_gj",   limit: 4
    t.integer  "qtzwxl_zj",   limit: 4
    t.integer  "qtzwxl_cj",   limit: 4
  end

  create_table "man_powers", force: :cascade do |t|
    t.integer  "school_id",            limit: 4
    t.string   "record_year",          limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "kjhdry_total",         limit: 4
    t.integer  "kjhdry_kxj_gcs",       limit: 4
    t.integer  "kjhdry_fzry",          limit: 4
    t.integer  "yjfzry_total",         limit: 4
    t.integer  "yjfzry_kxj_gcs",       limit: 4
    t.integer  "yjfzry_fzry",          limit: 4
    t.integer  "yffzqsry_total",       limit: 4
    t.integer  "yffzqsry_kxj_gcs",     limit: 4
    t.integer  "yffzqsry_fzry",        limit: 4
    t.integer  "cjyykjfwry_total",     limit: 4
    t.integer  "cjyykjfwry_kxj_gcs",   limit: 4
    t.integer  "cjyykjfwry_fzry",      limit: 4
    t.integer  "cjyykjfwqsry_total",   limit: 4
    t.integer  "cjyykjfwqsry_kxj_gcs", limit: 4
    t.integer  "cjyykjfwqsry_fzry",    limit: 4
  end

  create_table "research_developments", force: :cascade do |t|
    t.integer  "school_id",    limit: 4
    t.string   "record_year",  limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "total_xms",    limit: 4
    t.integer  "total_dntrrs", limit: 4
    t.integer  "total_zdyjs",  limit: 4
    t.integer  "total_dnbrjf", limit: 4
    t.integer  "total_dnzcjf", limit: 4
    t.integer  "jcyj_xms",     limit: 4
    t.integer  "jcyj_dntrrs",  limit: 4
    t.integer  "jcyj_zdyjs",   limit: 4
    t.integer  "jcyj_dnbrjf",  limit: 4
    t.integer  "jcyj_dnzcjf",  limit: 4
    t.integer  "yyyj_xms",     limit: 4
    t.integer  "yyyj_dntrrs",  limit: 4
    t.integer  "yyyj_zdyjs",   limit: 4
    t.integer  "yyyj_dnbrjf",  limit: 4
    t.integer  "yyyj_dnzcjf",  limit: 4
    t.integer  "syfz_xms",     limit: 4
    t.integer  "syfz_dntrrs",  limit: 4
    t.integer  "syfz_zdyjs",   limit: 4
    t.integer  "syfz_dnbrjf",  limit: 4
    t.integer  "syfz_dnzcjf",  limit: 4
  end

  create_table "school_tags", force: :cascade do |t|
    t.string   "tag",        limit: 255
    t.integer  "school_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "schools", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "en_name",    limit: 255
    t.string   "province",   limit: 255
    t.string   "city",       limit: 255
    t.string   "category",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "science_achievements", force: :cascade do |t|
    t.integer  "school_id",           limit: 4
    t.string   "record_year",         limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "cbkjzz_number",       limit: 4
    t.integer  "cbkjzz_zs",           limit: 4
    t.integer  "fbxslw_total",        limit: 4
    t.integer  "cbkjzz_gwxskw",       limit: 4
    t.integer  "gjjxmys_total",       limit: 4
    t.integer  "gjjxmys_ywdwhz",      limit: 4
    t.integer  "gjjxmys_xmly_973jh",  limit: 4
    t.integer  "gjjxmys_xmly_kjggjh", limit: 4
    t.integer  "gjjxmys_xmly_863jh",  limit: 4
    t.integer  "gjjxmys_xmly_zrkxjj", limit: 4
    t.integer  "gjjxmys_xmly_other",  limit: 4
    t.integer  "zlqk_zlsqs_total",    limit: 4
    t.integer  "zlqk_zlsqs_fmzl",     limit: 4
    t.integer  "zlqk_zlsqs_syxx",     limit: 4
    t.integer  "zlqk_zlsqs_wgsj",     limit: 4
    t.integer  "zlqk_zlsqshu_total",  limit: 4
    t.integer  "zlqk_zlsqshu_fmzl",   limit: 4
    t.integer  "zlqk_zlsqshu_syxx",   limit: 4
    t.integer  "zlqk_zlsqshu_wgsj",   limit: 4
    t.integer  "zlqk_zlsqshu_qtzscq", limit: 4
    t.integer  "zlcss_hts",           limit: 4
    t.integer  "zlcss_zje",           limit: 4
    t.integer  "zlcss_dnsjsr",        limit: 4
    t.integer  "qtzscq",              limit: 4
  end

  create_table "science_funds", force: :cascade do |t|
    t.integer  "school_id",   limit: 4
    t.string   "record_year", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "br_total",    limit: 4
    t.integer  "br_zfzj",     limit: 4
    t.integer  "br_qsydwwt",  limit: 4
    t.integer  "br_qtjf",     limit: 4
    t.integer  "zc_total",    limit: 4
    t.integer  "zc_lwf",      limit: 4
    t.integer  "zc_ywf",      limit: 4
    t.integer  "zc_zbwdw",    limit: 4
    t.integer  "zc_other",    limit: 4
  end

end
