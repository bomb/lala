class CreateSchoolTags < ActiveRecord::Migration
  def change
    create_table :school_tags do |t|
      t.string :tag
      t.integer :school_id

      t.timestamps null: false
    end
  end
end
