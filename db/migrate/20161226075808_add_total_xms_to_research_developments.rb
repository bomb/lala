class AddTotalXmsToResearchDevelopments < ActiveRecord::Migration
  def change
    add_column :research_developments, :total_xms, :integer
    add_column :research_developments, :total_dntrrs, :integer
    add_column :research_developments, :total_zdyjs, :integer
    add_column :research_developments, :total_dnbrjf, :integer
    add_column :research_developments, :total_dnzcjf, :integer

    add_column :research_developments, :jcyj_xms, :integer
    add_column :research_developments, :jcyj_dntrrs, :integer
    add_column :research_developments, :jcyj_zdyjs, :integer
    add_column :research_developments, :jcyj_dnbrjf, :integer
    add_column :research_developments, :jcyj_dnzcjf, :integer

    add_column :research_developments, :yyyj_xms, :integer
    add_column :research_developments, :yyyj_dntrrs, :integer
    add_column :research_developments, :yyyj_zdyjs, :integer
    add_column :research_developments, :yyyj_dnbrjf, :integer
    add_column :research_developments, :yyyj_dnzcjf, :integer

    add_column :research_developments, :syfz_xms, :integer
    add_column :research_developments, :syfz_dntrrs, :integer
    add_column :research_developments, :syfz_zdyjs, :integer
    add_column :research_developments, :syfz_dnbrjf, :integer
    add_column :research_developments, :syfz_dnzcjf, :integer
  end
end
