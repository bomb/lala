class CreateScienceAchievements < ActiveRecord::Migration
  def change
    create_table :science_achievements do |t|
      t.integer :school_id
      t.string :record_year

      t.timestamps null: false
    end
  end
end
