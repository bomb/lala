class CreateCommunications < ActiveRecord::Migration
  def change
    create_table :communications do |t|
      t.integer :school_id
      t.string :record_year

      t.timestamps null: false
    end
  end
end
