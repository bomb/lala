class AddCbkjzzNumberToScienceAchievements < ActiveRecord::Migration
  def change
    add_column :science_achievements, :cbkjzz_number, :integer
    add_column :science_achievements, :cbkjzz_zs, :integer

    add_column :science_achievements, :fbxslw_total, :integer
    add_column :science_achievements, :cbkjzz_gwxskw, :integer

    add_column :science_achievements, :gjjxmys_total, :integer
    add_column :science_achievements, :gjjxmys_ywdwhz, :integer
    add_column :science_achievements, :gjjxmys_xmly_973jh, :integer
    add_column :science_achievements, :gjjxmys_xmly_kjggjh, :integer
    add_column :science_achievements, :gjjxmys_xmly_863jh, :integer
    add_column :science_achievements, :gjjxmys_xmly_zrkxjj, :integer
    add_column :science_achievements, :gjjxmys_xmly_other, :integer

    add_column :science_achievements, :zlqk_zlsqs_total, :integer
    add_column :science_achievements, :zlqk_zlsqs_fmzl, :integer
    add_column :science_achievements, :zlqk_zlsqs_syxx, :integer
    add_column :science_achievements, :zlqk_zlsqs_wgsj, :integer

    add_column :science_achievements, :zlqk_zlsqshu_total, :integer
    add_column :science_achievements, :zlqk_zlsqshu_fmzl, :integer
    add_column :science_achievements, :zlqk_zlsqshu_syxx, :integer
    add_column :science_achievements, :zlqk_zlsqshu_wgsj, :integer
    add_column :science_achievements, :zlqk_zlsqshu_qtzscq, :integer

    add_column :science_achievements, :zlcss_hts, :integer
    add_column :science_achievements, :zlcss_zje, :integer
    add_column :science_achievements, :zlcss_dnsjsr, :integer

    add_column :science_achievements, :qtzscq, :integer
  end
end
