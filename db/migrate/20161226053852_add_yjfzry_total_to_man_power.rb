class AddYjfzryTotalToManPower < ActiveRecord::Migration
  def change
    add_column :man_powers, :yjfzry_total, :integer
    add_column :man_powers, :yjfzry_kxj_gcs, :integer
    add_column :man_powers, :yjfzry_fzry, :integer

    add_column :man_powers, :yffzqsry_total, :integer
    add_column :man_powers, :yffzqsry_kxj_gcs, :integer
    add_column :man_powers, :yffzqsry_fzry, :integer

    add_column :man_powers, :cjyykjfwry_total, :integer
    add_column :man_powers, :cjyykjfwry_kxj_gcs, :integer
    add_column :man_powers, :cjyykjfwry_fzry, :integer

    add_column :man_powers, :cjyykjfwqsry_total, :integer
    add_column :man_powers, :cjyykjfwqsry_kxj_gcs, :integer
    add_column :man_powers, :cjyykjfwqsry_fzry, :integer
  end
end
