class CreateResearchDevelopments < ActiveRecord::Migration
  def change
    create_table :research_developments do |t|
      t.integer :school_id
      t.string :record_year

      t.timestamps null: false
    end
  end
end
