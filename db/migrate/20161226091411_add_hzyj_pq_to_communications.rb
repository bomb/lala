class AddHzyjPqToCommunications < ActiveRecord::Migration
  def change
    add_column :communications, :hzjl_pq, :integer
    add_column :communications, :hzjl_js, :integer

    add_column :communications, :gjxshy_cxry, :integer
    add_column :communications, :hzjl_jllw, :integer
    add_column :communications, :hzjl_tybg, :integer
    add_column :communications, :hzjl_zb, :integer
  end
end
