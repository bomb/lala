class CreateManDuties < ActiveRecord::Migration
  def change
    create_table :man_duties do |t|
      t.integer :school_id
      t.string :record_year

      t.timestamps null: false
    end
  end
end
