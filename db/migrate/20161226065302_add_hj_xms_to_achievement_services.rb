class AddHjXmsToAchievementServices < ActiveRecord::Migration
  def change
    add_column :achievement_services, :hj_xms, :integer
    add_column :achievement_services, :hj_dntrrs, :integer
    add_column :achievement_services, :hj_zdyjs, :integer
    add_column :achievement_services, :hj_dnbrjf, :integer
    add_column :achievement_services, :hj_dnzcjf, :integer

    add_column :achievement_services, :yjyfzcgyy_xms, :integer
    add_column :achievement_services, :yjyfzcgyy_dntrrs, :integer
    add_column :achievement_services, :yjyfzcgyy_zdyjs, :integer
    add_column :achievement_services, :yjyfzcgyy_dnbrjf, :integer
    add_column :achievement_services, :yjyfzcgyy_dnzcjf, :integer

    add_column :achievement_services, :kjfw_xms, :integer
    add_column :achievement_services, :kjfw_dntrrs, :integer
    add_column :achievement_services, :kjfw_zdyjs, :integer
    add_column :achievement_services, :kjfw_dnbrjf, :integer
    add_column :achievement_services, :kjfw_dnzcjf, :integer
  end
end
