class AddHtsTotalToAssignment < ActiveRecord::Migration
  def change
    add_column :assignments, :hts_total, :integer
    add_column :assignments, :hts_gyqy, :integer
    add_column :assignments, :hts_wzqy, :integer
    add_column :assignments, :hts_myqy, :integer
    add_column :assignments, :hts_other, :integer

    add_column :assignments, :htje_total, :integer
    add_column :assignments, :htje_gyqy, :integer
    add_column :assignments, :htje_wzqy, :integer
    add_column :assignments, :htje_myqy, :integer
    add_column :assignments, :htje_other, :integer
    
    add_column :assignments, :dnsjsr_total, :integer
    add_column :assignments, :dnsjsr_gyqy, :integer
    add_column :assignments, :dnsjsr_wzqy, :integer
    add_column :assignments, :dnsjsr_myqy, :integer
    add_column :assignments, :dnsjsr_other, :integer
  end
end
