class CreateActivityProfiles < ActiveRecord::Migration
  def change
    create_table :activity_profiles do |t|
      t.integer :school_id
      t.string :record_year
      t.integer :jxykyry_total
      t.integer :jxykyry_kxj_gcs_total
      t.integer :jxykyry_kxj_gcs_gjzc

      t.integer :yjyfzry_total
      t.integer :yjyfzry_kxj_gcs_total
      t.integer :yjyfzry_kxj_gcs_gjzc
      t.integer :yjyfzry_qsdlry

      t.integer :kjjf_total 
      t.integer :kjjf_dnbr_total 
      t.integer :kjjf_dnbr_zfzj
      t.integer :kjjf_dnbr_qsydwwt
      t.integer :kjjf_dnbr_other
      t.integer :kjjf_dnnbzc

      t.integer :kjkt_ktzs
      t.integer :kjkt_dntrrs
      t.integer :kjkt_dnbrjf
      t.integer :kjkt_dnzcjf

      t.integer :kjcgjjszr_zz_sl
      t.integer :kjcgjjszr_zz_zs
      t.integer :kjcgjjszr_xslw_total
      t.integer :kjcgjjszr_xslw_gwjqgxkwfb
      t.integer :kjcgjjszr_jdcgs
      t.integer :kjcgjjszr_jszr_qdhts
      t.integer :kjcgjjszr_jszr_dnsjsr

      t.integer :cgsj_total
      t.integer :cgsj_gjjj

      t.timestamps null: false
    end
  end
end
