class AddTotalToManDuties < ActiveRecord::Migration
  def change
    add_column :man_duties, :total, :integer

    add_column :man_duties, :jsxl_xj, :integer
    add_column :man_duties, :jsxl_jshi, :integer
    add_column :man_duties, :jsxl_fjs, :integer
    add_column :man_duties, :jsxl_jshou, :integer
    add_column :man_duties, :jsxl_zs, :integer
    add_column :man_duties, :jsxl_other, :integer

    add_column :man_duties, :qtzwxl_xj, :integer
    add_column :man_duties, :qtzwxl_gj, :integer
    add_column :man_duties, :qtzwxl_zj, :integer
    add_column :man_duties, :qtzwxl_cj, :integer
  end
end
