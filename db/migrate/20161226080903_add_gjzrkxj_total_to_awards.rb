class AddGjzrkxjTotalToAwards < ActiveRecord::Migration
  def change
    add_column :awards, :gjzgkxjsj_total, :integer
    add_column :awards, :gjzgkxjsj_td, :integer

    add_column :awards, :gjzrkxj_total, :integer
    add_column :awards, :gjzrkxj_yd, :integer
    add_column :awards, :gjzrkxj_ed, :integer

    add_column :awards, :gjfmj_total, :integer
    add_column :awards, :gjfmj_yd, :integer
    add_column :awards, :gjfmj_ed, :integer
 
    add_column :awards, :gjkjjbj_total, :integer
    add_column :awards, :gjkjjbj_td, :integer
    add_column :awards, :gjkjjbj_yd, :integer
    add_column :awards, :gjkjjbj_ed, :integer
    add_column :awards, :gjkjjbj_sd, :integer

    add_column :awards, :gwygbm_kjjbj, :integer
    add_column :awards, :sszzq_kjjbj, :integer
  end
end