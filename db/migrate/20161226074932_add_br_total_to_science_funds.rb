class AddBrTotalToScienceFunds < ActiveRecord::Migration
  def change
    add_column :science_funds, :br_total, :integer
    add_column :science_funds, :br_zfzj, :integer
    add_column :science_funds, :br_qsydwwt, :integer
    add_column :science_funds, :br_qtjf, :integer

    add_column :science_funds, :zc_total, :integer
    add_column :science_funds, :zc_lwf, :integer
    add_column :science_funds, :zc_ywf, :integer
    add_column :science_funds, :zc_zbwdw, :integer
    add_column :science_funds, :zc_other, :integer
  end
end
