class CreateAwards < ActiveRecord::Migration
  def change
    create_table :awards do |t|
      t.integer :school_id
      t.string :record_year

      t.timestamps null: false
    end
  end
end
