#coding:utf-8
#科技交流
# == Schema Information
#
# Table name: communications
#
#  id          :integer          not null  primary key
#  school_id   :integer
#  record_year :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
# 合作研究
#  hzjl_pq  :integer  派遣(人次)
#  hzjl_js  :integer  接受(人次)

# 国际学术会议
#  gjxshy_cxry  :integer  出席人员(人次)
#  hzjl_jllw    :integer  交流论文(篇)
#  hzjl_tybg    :integer  特邀报告(篇)
#  hzjl_zb      :integer  主办(次)
#

class Communication < ActiveRecord::Base

	def self.table_head
		{
		  "2005" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"],
		  "2006" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"],
		  "2007" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"],
		  "2008" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"],
		  "2009" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"],
		  "2010" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"],
		  "2011" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"],
		  "2012" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"],
		  "2013" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"],
		  "2014" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"],
		  "2015" => ["hzjl_pq", "hzjl_js", "gjxshy_cxry", "hzjl_jllw", "hzjl_tybg", "hzjl_zb"]
		}
	end
	
end
