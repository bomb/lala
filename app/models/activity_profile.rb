#coding:utf-8
#成果应用及科技服务项目
# == Schema Information
#
# Table name: activity_profiles
#  id          :integer          not null  primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null

#  school_id                    :integer       学校id
#  record_year                  :string(255)   年份

# 教学与科研人员(人)
#  jxykyry_total                :integer    合计
#  jxykyry_kxj_gcs_total        :integer    科学家与工程师小计
#  jxykyry_kxj_gcs_gjzc         :integer    科学家与工程师高级职称

# 研究与发展人员
#  yjyfzry_total                :integer    合计
#  yjyfzry_kxj_gcs_total        :integer    科学家与工程师小计
#  yjyfzry_kxj_gcs_gjzc         :integer    科学家与工程师高级职称
#  yjyfzry_qsdlry               :integer    全时当量人员(人年)

# 科技经费(千元)
#  kjjf_total                   :integer    合计
#  kjjf_dnbr_total              :integer    当年拨入/小计
#  kjjf_dnbr_zfzj               :integer    当年拨入/政府资金
#  kjjf_dnbr_qsydwwt            :integer    当年拨入/企事业单位委托(进入学校财务)
#  kjjf_dnbr_other              :integer    当年拨入/其他
#  kjjf_dnnbzc                  :integer    当年内部指出

# 科技课题
#  kjkt_ktzs                    :integer    课题总数(项)
#  kjkt_dntrrs                  :integer    当年投入人数(人)
#  kjkt_dnbrjf                  :integer    当年拨入经费(千元)
#  kjkt_dnzcjf                  :integer    当年支出经费(千元)

# 科技成果及技术转让
#  kjcgjjszr_zz_sl              :integer    专著/数量(部)
#  kjcgjjszr_zz_zs              :integer    专著/字数(千字)
#  kjcgjjszr_xslw_total         :integer    学术论文(篇)/合计
#  kjcgjjszr_xslw_gwjqgxkwfb    :integer    学术论文(篇)/国外及全国性刊物发表
#  kjcgjjszr_jdcgs              :integer    鉴定成果数(项)
#  kjcgjjszr_jszr_qdhts         :integer    技术转让/鉴定合同数(项)
#  kjcgjjszr_jszr_dnsjsr        :integer    技术转让/当年实际收入(千元)

# 成果授奖(项)
#  cgsj_total                   :integer    合计
#  cgsj_gjjj                    :integer    其中:国家级奖

class ActivityProfile < ActiveRecord::Base
	def self.table_head
		{
		  "2005" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"],
		  "2006" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_total", "kjjf_dnbr_total", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"],
		  "2007" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_total", "kjjf_dnbr_total", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"],
		  "2008" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_total", "kjjf_dnbr_total", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"],
		  "2009" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_total", "kjjf_dnbr_total", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"],
		  "2010" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_total", "kjjf_dnbr_total", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"],
		  "2011" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_total", "kjjf_dnbr_total", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"],
		  "2012" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_total", "kjjf_dnbr_total", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"],
		  "2013" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_total", "kjjf_dnbr_total", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"],
		  "2014" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_total", "kjjf_dnbr_total", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"],
		  "2015" => ["jxykyry_total", "jxykyry_kxj_gcs_total", "jxykyry_kxj_gcs_gjzc", "yjyfzry_total", "yjyfzry_kxj_gcs_total", "yjyfzry_kxj_gcs_gjzc", "yjyfzry_qsdlry", "kjjf_total", "kjjf_dnbr_total", "kjjf_dnbr_zfzj", "kjjf_dnbr_qsydwwt", "kjjf_dnbr_other", "kjjf_dnnbzc", "kjkt_ktzs", "kjkt_dntrrs", "kjkt_dnbrjf", "kjkt_dnzcjf", "kjcgjjszr_zz_sl", "kjcgjjszr_zz_zs", "kjcgjjszr_xslw_total", "kjcgjjszr_xslw_gwjqgxkwfb", "kjcgjjszr_jdcgs", "kjcgjjszr_jszr_qdhts", "kjcgjjszr_jszr_dnsjsr", "cgsj_total", "cgsj_gjjj"]
		}
  end
end
