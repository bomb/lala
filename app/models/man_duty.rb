#coding:utf-8
#科技人员职称表
# == Schema Information
#
# Table name: man_duties
#
#  id          :integer          not null  primary key
#  school_id   :integer
#  record_year :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null

#  total       :integer  合计
# 
# 教师系列
#  jsxl_xj     :integer  小计
#  jsxl_jshi     :integer  教授
#  jsxl_fjs    :integer  副教授
#  jsxl_jshou     :integer  讲师
#  jsxl_zs     :integer  助教
#  jsxl_other  :integer  其他
# 
# 其他技术职务系列
#  qtzwxl_xj   :integer  小计
#  qtzwxl_gj   :integer  高级
#  qtzwxl_zj   :integer  中级
#  qtzwxl_cj   :integer  初级
#


class ManDuty < ActiveRecord::Base

	def self.table_head
		{
		  "2005" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"],
		  "2006" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"],
		  "2007" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"],
		  "2008" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"],
		  "2009" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"],
		  "2010" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"],
		  "2011" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"],
		  "2012" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"],
		  "2013" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"],
		  "2014" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"],
		  "2015" => ["total", "jsxl_xj", "jsxl_jshi", "jsxl_fjs", "jsxl_jshou", "jsxl_zs", "jsxl_other", "qtzwxl_xj", "qtzwxl_gj", "qtzwxl_zj", "qtzwxl_cj"]
		}
	end
end
