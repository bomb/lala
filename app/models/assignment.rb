#coding:utf-8
#技术转让
# == Schema Information
#
# Table name: assignments
#
#  id          :integer          not null   primary key
#  school_id   :integer
#  record_year :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
# 合同数(项）
#  hts_total   :integer  合计
#  hts_gyqy    :integer  国有企业
#  hts_wzqy    :integer  外资企业
#  hts_myqy    :integer  民营企业
#  hts_other   :integer  其它

# 合同金额(千元)
#  htje_total  :integer  合计
#  htje_gyqy   :integer  国有企业
#  htje_wzqy   :integer  外资企业
#  htje_myqy   :integer  民营企业
#  htje_other  :integer  其它

# 当年实际收入(千元)
#  dnsjsr_total  :integer  合计
#  dnsjsr_gyqy   :integer  国有企业
#  dnsjsr_wzqy   :integer  外资企业
#  dnsjsr_myqy   :integer  民营企业
#  dnsjsr_other  :integer  其它
#

class Assignment < ActiveRecord::Base

	def self.table_head
    {
      "2005" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"],
      "2006" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"],
      "2007" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"],
      "2008" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"],
      "2009" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"],
      "2010" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"],
      "2011" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"],
      "2012" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"],
      "2013" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"],
      "2014" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"],
      "2015" => ["hts_total", "hts_gyqy", "hts_wzqy", "hts_myqy", "hts_other", "htje_total", "htje_gyqy", "htje_wzqy", "htje_myqy", "htje_other", "dnsjsr_total", "dnsjsr_gyqy", "dnsjsr_wzqy", "dnsjsr_myqy", "dnsjsr_other"]
    }
  end
  
end
