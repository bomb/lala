#coding:utf-8
#科技成果
# == Schema Information
#
# Table name: science_achievements
#
#  id          :integer          not null  primary key
#  school_id   :integer
#  record_year :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
# 出版科技著作
#  cbkjzz_number  :integer 数量(部)
#  cbkjzz_zs      :integer 字数(千字)

# 发表学术论文(篇)
#  fbxslw_total   :integer 合计
#  cbkjzz_gwxskw  :integer 其中:国外学术刊物

# 国家级项目验收(项)
#  gjjxmys_total       :integer 合计
#  gjjxmys_ywdwhz      :integer 与外单位合作
#  gjjxmys_xmly_973jh  :integer 973计划
#  gjjxmys_xmly_kjggjh :integer 科技攻关计划
#  gjjxmys_xmly_863jh  :integer 863计划
#  gjjxmys_xmly_zrkxjj :integer 自然科学基金
#  gjjxmys_xmly_other  :integer 其他

# 专利情况=>专利申请数(件)
#  zlqk_zlsqs_total :integer 合计
#  zlqk_zlsqs_fmzl  :integer 发明专利
#  zlqk_zlsqs_syxx  :integer 实用新型
#  zlqk_zlsqs_wgsj  :integer 外观设计

# 专利情况=>专利授权数(件)
#  zlqk_zlsqshu_total :integer 合计
#  zlqk_zlsqshu_fmzl  :integer 发明专利
#  zlqk_zlsqshu_syxx  :integer 实用新型
#  zlqk_zlsqshu_wgsj  :integer 外观设计
#  zlqk_zlsqshu_qtzscq  :integer 其他知识产权

# 专利情况=>专利出售数
#  zlcss_hts     :integer 合同数(项)
#  zlcss_zje     :integer 总金额(千元)
#  zlcss_dnsjsr  :integer 当年实际收入(千元)

#  qtzscq  :integer 其他知识产权
#

class ScienceAchievement < ActiveRecord::Base
	
	def self.table_head
		{
		  "2005" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_qtzscq", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr"],
		  "2006" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr", "qtzscq"],
		  "2007" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr", "qtzscq"],
		  "2008" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr", "qtzscq"],
		  "2009" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr", "qtzscq"],
		  "2010" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr", "qtzscq"],
		  "2011" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr", "qtzscq"],
		  "2012" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr", "qtzscq"],
		  "2013" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr", "qtzscq"],
		  "2014" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr", "qtzscq"],
		  "2015" => ["cbkjzz_number", "cbkjzz_zs", "fbxslw_total", "cbkjzz_gwxskw", "gjjxmys_total", "gjjxmys_ywdwhz", "gjjxmys_xmly_973jh", "gjjxmys_xmly_kjggjh", "gjjxmys_xmly_863jh", "gjjxmys_xmly_zrkxjj", "gjjxmys_xmly_other", "zlqk_zlsqs_total", "zlqk_zlsqs_fmzl", "zlqk_zlsqs_syxx", "zlqk_zlsqs_wgsj", "zlqk_zlsqshu_total", "zlqk_zlsqshu_fmzl", "zlqk_zlsqshu_syxx", "zlqk_zlsqshu_wgsj", "zlcss_hts", "zlcss_zje", "zlcss_dnsjsr", "qtzscq"]
		}
  end

end
