#coding:utf-8
#科研经费拨入，拨出
# == Schema Information
#
# Table name: science_funds
#
#  id          :integer          not null  primary key
#  school_id   :integer
#  record_year :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
# 拨入
#  br_total    :integer  合计
#  br_zfzj     :integer  政府资金
#  br_qsydwwt  :integer  企事业单位委托
#  br_qtjf     :integer  其他经费

# 支出
#  zc_total  :integer  合计
#  zc_lwf    :integer  劳务费
#  zc_ywf    :integer  业务费
#  zc_zbwdw  :integer  转拨外单位
#  zc_other  :integer  其他
#

class ScienceFund < ActiveRecord::Base
	belongs_to :school

	def self.table_head
		{
		  "2005" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"],
		  "2006" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"],
		  "2007" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"],
		  "2008" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"],
		  "2009" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"],
		  "2010" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"],
		  "2011" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"],
		  "2012" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"],
		  "2013" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"],
		  "2014" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"],
		  "2015" => ["br_total", "br_zfzj", "br_qsydwwt", "br_qtjf", "zc_total", "zc_lwf", "zc_ywf", "zc_zbwdw", "zc_other"]
		}
  end
end
