#coding:utf-8
# == Schema Information
#
# Table name: man_powers
#
#  id             :integer          not null primary key
#  school_id      :integer
#  record_year    :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
# 科技活动人员(人)
#  kjhdry_total   :integer   合计
#  kjhdry_kxj_gcs :integer   科学家和工程师
#  kjhdry_fzry    :integer   辅助人员

# 研究与发展人员(人)
#  yjfzry_total   :integer   合计
#  yjfzry_kxj_gcs :integer   科学家和工程师
#  yjfzry_fzry    :integer   辅助人员

# 研究与发展全时人员(人年)
#  yffzqsry_total   :integer   合计
#  yffzqsry_kxj_gcs :integer   科学家和工程师
#  yffzqsry_fzry    :integer   辅助人员

# R&D成果应用及科技服务人员(人)
#  cjyykjfwry_total   :integer   合计
#  cjyykjfwry_kxj_gcs :integer   科学家和工程师
#  cjyykjfwry_fzry    :integer   辅助人员

# R&D成果应用及科技服务全时人员(人年)
#  cjyykjfwqsry_total   :integer   合计
#  cjyykjfwqsry_kxj_gcs :integer   科学家和工程师
#  cjyykjfwqsry_fzry    :integer   辅助人员
#

class ManPower < ActiveRecord::Base
	belongs_to :school

	def self.table_head
    {
      "2005" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"],
      "2006" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"],
      "2007" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"],
      "2008" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"],
      "2009" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"],
      "2010" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"],
      "2011" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"],
      "2012" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"],
      "2013" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"],
      "2014" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"],
      "2015" => ["kjhdry_total", "kjhdry_kxj_gcs", "kjhdry_fzry", "yjfzry_total", "yjfzry_kxj_gcs", "yjfzry_fzry", "yffzqsry_total", "yffzqsry_kxj_gcs", "yffzqsry_fzry", "cjyykjfwry_total", "cjyykjfwry_kxj_gcs", "cjyykjfwry_fzry", "cjyykjfwqsry_total", "cjyykjfwqsry_kxj_gcs", "cjyykjfwqsry_fzry"]
    }
  end

end