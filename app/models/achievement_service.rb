#coding:utf-8
#成果应用及科技服务项目
# == Schema Information
#
# Table name: achievement_services
#
#  id          :integer          not null  primary key
#  school_id   :integer
#  record_year :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
# 合 计
#  hj_xms      :integer  项目数(项)
#  hj_dntrrs   :integer  当年投入人数(人年)
#  hj_zdyjs    :integer  在读研究生（人）
#  hj_dnbrjf   :integer  当年拨入经费（千元）
#  hj_dnzcjf   :integer  当年支出经费（千元）

# 研究与发展成果应用
#  yjyfzcgyy_xms     :integer  项目数(项)
#  yjyfzcgyy_dntrrs  :integer  当年投入人数(人年)
#  yjyfzcgyy_zdyjs   :integer  在读研究生（人）
#  yjyfzcgyy_dnbrjf  :integer  当年拨入经费（千元）
#  yjyfzcgyy_dnzcjf  :integer  当年支出经费（千元）

# 科技服务
#  kjfw_xms      :integer  项目数(项)
#  kjfw_dntrrs   :integer  当年投入人数(人年)
#  kjfw_zdyjs    :integer  在读研究生（人）
#  kjfw_dnbrjf   :integer  当年拨入经费（千元）
#  kjfw_dnzcjf   :integer  当年支出经费（千元）
#

class AchievementService < ActiveRecord::Base

	def self.table_head
		{
		  "2005" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"],
		  "2006" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"],
		  "2007" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"],
		  "2008" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"],
		  "2009" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"],
		  "2010" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"],
		  "2011" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"],
		  "2012" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"],
		  "2013" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"],
		  "2014" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"],
		  "2015" => ["hj_xms", "hj_dntrrs", "hj_zdyjs", "hj_dnbrjf", "hj_dnzcjf", "yjyfzcgyy_xms", "yjyfzcgyy_dntrrs", "yjyfzcgyy_zdyjs", "yjyfzcgyy_dnbrjf", "yjyfzcgyy_dnzcjf", "kjfw_xms", "kjfw_dntrrs", "kjfw_zdyjs", "kjfw_dnbrjf", "kjfw_dnzcjf"]
		}

	end
end
