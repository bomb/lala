#coding:utf-8
#成果获奖
# == Schema Information
#
# Table name: awards
#
#  id          :integer          not null  primary key
#  school_id   :integer
#  record_year :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
# 国家最高科学技术奖
#  gjzgkxjsj_total  :integer  合计
#  gjzgkxjsj_td     :integer  特等
# 国家自然科学奖
#  gjzrkxj_total  :integer  合计
#  gjzrkxj_yd     :integer  一等 +++
#  gjzrkxj_ed     :integer  二等

# 国家发明奖
#  gjfmj_total    :integer  合计
#  gjfmj_yd       :integer  一等 +++
#  gjfmj_ed       :integer  二等

# 国家科技进步奖
#  gjkjjbj_total  :integer  合计
#  gjkjjbj_td     :integer  特等
#  gjkjjbj_yd     :integer  一等
#  gjkjjbj_ed     :integer  二等
#  gjkjjbj_sd     :integer  三等 +++

#  gwygbm_kjjbj   :integer  国务院各部门科技进步奖
#  sszzq_kjjbj    :integer  省、市、自治区、科技进步奖
#

class Award < ActiveRecord::Base

  def self.table_head
  	{
  	  "2005" => ["gjzrkxj_total", "gjzrkxj_ed", "gjfmj_total", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_yd", "gjkjjbj_ed", "gwygbm_kjjbj", "sszzq_kjjbj"],
  	  "2006" => ["gjzrkxj_total", "gjzrkxj_yd", "gjzrkxj_ed", "gjfmj_total", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_yd", "gjkjjbj_ed", "gjkjjbj_sd", "gwygbm_kjjbj", "sszzq_kjjbj"],
  	  "2007" => ["gjzrkxj_total", "gjzrkxj_yd", "gjzrkxj_ed", "gjfmj_total", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_yd", "gjkjjbj_ed", "gjkjjbj_sd", "gwygbm_kjjbj", "sszzq_kjjbj"],
  	  "2008" => ["gjzgkxjsj_total", "gjzgkxjsj_td", "gjzrkxj_total", "gjzrkxj_ed", "gjfmj_total", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_td", "gjkjjbj_yd", "gjkjjbj_ed", "gwygbm_kjjbj", "sszzq_kjjbj"],
  	  "2009" => ["gjzgkxjsj_total", "gjzgkxjsj_td", "gjzrkxj_total", "gjzrkxj_ed", "gjfmj_total", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_td", "gjkjjbj_yd", "gjkjjbj_ed", "gwygbm_kjjbj", "sszzq_kjjbj"],
  	  "2010" => ["gjzgkxjsj_total", "gjzgkxjsj_td", "gjzrkxj_total", "gjzrkxj_ed", "gjfmj_total", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_td", "gjkjjbj_yd", "gjkjjbj_ed", "gwygbm_kjjbj", "sszzq_kjjbj"],
  	  "2011" => ["gjzrkxj_total", "gjzrkxj_ed", "gjfmj_total", "gjfmj_yd", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_td", "gjkjjbj_yd", "gjkjjbj_ed", "gwygbm_kjjbj", "sszzq_kjjbj"],
  	  "2012" => ["gjzrkxj_total", "gjzrkxj_yd", "gjzrkxj_ed", "gjfmj_total", "gjfmj_yd", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_td", "gjkjjbj_yd", "gjkjjbj_ed", "gwygbm_kjjbj", "sszzq_kjjbj"],
  	  "2013" => ["gjzrkxj_total", "gjzrkxj_yd", "gjzrkxj_ed", "gjfmj_total", "gjfmj_yd", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_td", "gjkjjbj_yd", "gjkjjbj_ed", "gwygbm_kjjbj", "sszzq_kjjbj"],
  	  "2014" => ["gjzrkxj_total", "gjzrkxj_yd", "gjzrkxj_ed", "gjfmj_total", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_td", "gjkjjbj_yd", "gjkjjbj_ed", "gwygbm_kjjbj", "sszzq_kjjbj"],
  	  "2015" => ["gjzrkxj_total", "gjzrkxj_ed", "gjfmj_total", "gjfmj_ed", "gjkjjbj_total", "gjkjjbj_td", "gjkjjbj_yd", "gjkjjbj_ed", "gwygbm_kjjbj", "sszzq_kjjbj"]
  	}
  end
  
end
