#coding:utf-8
#研究与发展项目
# == Schema Information
#
# Table name: research_developments
#
#  id          :integer          not null, primary key
#  school_id   :integer
#  record_year :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null

# 合  计
#  total_xms  :integer      项目数(项)
#  total_dntrrs  :integer   当年投入人数(人年)
#  total_zdyjs  :integer    在读研究生(人)
#  total_dnbrjf  :integer   当年拨入经费(千元)
#  total_dnzcjf  :integer   当年支出经费(千元)

# 基础研究
#  jcyj_xms  :integer      项目数(项)
#  jcyj_dntrrs  :integer   当年投入人数(人年)
#  jcyj_zdyjs  :integer    在读研究生(人)
#  jcyj_dnbrjf  :integer   当年拨入经费(千元)
#  jcyj_dnzcjf  :integer   当年支出经费(千元)

# 应用研究
#  yyyj_xms  :integer      项目数(项)
#  yyyj_dntrrs  :integer   当年投入人数(人年)
#  yyyj_zdyjs  :integer    在读研究生(人)
#  yyyj_dnbrjf  :integer   当年拨入经费(千元)
#  yyyj_dnzcjf  :integer   当年支出经费(千元)

# 试验发展
#  syfz_xms  :integer      项目数(项)
#  syfz_dntrrs  :integer   当年投入人数(人年)
#  syfz_zdyjs  :integer    在读研究生(人)
#  syfz_dnbrjf  :integer   当年拨入经费(千元)
#  syfz_dnzcjf  :integer   当年支出经费(千元)
#

class ResearchDevelopment < ActiveRecord::Base
	
	def self.table_head
		{
		  "2005" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"],
		  "2006" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"],
		  "2007" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"],
		  "2008" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"],
		  "2009" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"],
		  "2010" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"],
		  "2011" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"],
		  "2012" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"],
		  "2013" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"],
		  "2014" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"],
		  "2015" => ["total_xms", "total_dntrrs", "total_zdyjs", "total_dnbrjf", "total_dnzcjf", "jcyj_xms", "jcyj_dntrrs", "jcyj_zdyjs", "jcyj_dnbrjf", "jcyj_dnzcjf", "yyyj_xms", "yyyj_dntrrs", "yyyj_zdyjs", "yyyj_dnbrjf", "yyyj_dnzcjf", "syfz_xms", "syfz_dntrrs", "syfz_zdyjs", "syfz_dnbrjf", "syfz_dnzcjf"]
		}
	end
	
end
