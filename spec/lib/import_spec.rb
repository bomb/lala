require "rails_helper"

describe Import do
	before(:all) do
		ActiveRecord::Tasks::DatabaseTasks.drop_current("test")
		ActiveRecord::Tasks::DatabaseTasks.create_current("test")
		ActiveRecord::Tasks::DatabaseTasks.migrate
		Import.import
	end

	it "should correct load the data from the file to the database" do
	  expect(AchievementService.where(record_year: 2005, school_id: 6).pluck(:hj_dnzcjf, :yjyfzcgyy_zdyjs,:yjyfzcgyy_xms)).to eq([[4133, 14, 37]])
	  expect(Assignment.where(record_year: 2013, school_id: 68).pluck(:htje_total, :dnsjsr_total,:dnsjsr_myqy)).to eq([[660, 460, 460]])
	  expect(Award.where(record_year: 2008, school_id: 39).pluck(:gjzrkxj_yd, :gjkjjbj_sd,:sszzq_kjjbj)).to eq([[nil, nil, 18]])
	  expect(Communication.where(record_year: 2013, school_id: 31).pluck(:gjxshy_cxry, :hzjl_js, :hzjl_zb)).to eq([[1674,712,45]])
	  expect(ScienceFund.where(record_year: 2014, school_id: 1).pluck(:br_zfzj, :br_qtjf, :zc_lwf)).to eq([[1866530, 24722, 173185]])
	end

  it "should load correct number of data from excel docs" do
  	expect(ScienceAchievement.count).to eq(702)
    expect(ManPower.count).to eq(703)
    expect(ResearchDevelopment.count).to eq(703)
    expect(Assignment.count).to eq(701)
    expect(ScienceFund.count).to eq(703)
    expect(ManDuty.count).to eq(705)
    expect(Communication.count).to eq(702)
    expect(Award.count).to eq(663)
    expect(AchievementService.count).to eq(656)
    expect(School.count).to eq(68)
  end

  it "should not duplicate objects on file loads" do
  	expect(ScienceAchievement.distinct.count).to eq(702)
    expect(ManPower.distinct.count).to eq(703)
    expect(ResearchDevelopment.distinct.count).to eq(703)
    expect(Assignment.distinct.count).to eq(701)
    expect(ScienceFund.distinct.count).to eq(703)
    expect(ManDuty.distinct.count).to eq(705)
    expect(Communication.distinct.count).to eq(702)
    expect(Award.distinct.count).to eq(663)
    expect(AchievementService.distinct.count).to eq(656)
    expect(School.distinct.count).to eq(68)
  end
end

describe Activityimporter do
	before(:all) do
		ActiveRecord::Tasks::DatabaseTasks.drop_current("test")
		ActiveRecord::Tasks::DatabaseTasks.create_current("test")
		ActiveRecord::Tasks::DatabaseTasks.migrate
		Activityimporter.import
	end

	it "should correct load the data from the file to the database" do
	  expect(ActivityProfile.where(record_year: 2005, school_id: 9).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[957, 900, 202399]])
	  expect(ActivityProfile.where(record_year: 2006, school_id: 10).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[1920, 1309, 281083]])
	  expect(ActivityProfile.where(record_year: 2007, school_id: 11).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[957, 0, 104897]])
	  expect(ActivityProfile.where(record_year: 2008, school_id: 12).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[1614, 0, 35113]])
	  expect(ActivityProfile.where(record_year: 2009, school_id: 13).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[1364, 0, 220704]])
	  expect(ActivityProfile.where(record_year: 2010, school_id: 14).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[1384, 120, 202423]])
	  expect(ActivityProfile.where(record_year: 2011, school_id: 15).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[559, 0, 254580]])
	  expect(ActivityProfile.where(record_year: 2012, school_id: 16).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[944, 335, 720544]])
	  expect(ActivityProfile.where(record_year: 2013, school_id: 17).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[961, 3000, 422045]])
	  expect(ActivityProfile.where(record_year: 2014, school_id: 18).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[1831, 1930, 336430]])
	  expect(ActivityProfile.where(record_year: 2015, school_id: 19).pluck(:jxykyry_total, :kjcgjjszr_jszr_dnsjsr,:kjkt_dnbrjf)).to eq([[2794, 14925, 2215516]])
	end

	it "should load correct number of data from excel docs" do
		expect(ActivityProfile.count).to eq(9900)
	end
end